<?php do_action( 'fl_content_close' ); ?>

	</div><!-- .fl-page-content -->
	<?php

	do_action( 'fl_after_content' );

	if ( FLTheme::has_footer() ) :

		?>
	<footer class="fl-page-footer-wrap" itemscope="itemscope" itemtype="https://schema.org/WPFooter">
		<?php

		do_action( 'fl_footer_wrap_open' );

		do_action( 'fl_before_footer_widgets' );

		FLTheme::footer_widgets();

		do_action( 'fl_after_footer_widgets' );
		do_action( 'fl_before_footer' );

		

		FLTheme::footer();

		

		do_action( 'fl_after_footer' );
		do_action( 'fl_footer_wrap_close' );

		?>
	</footer>
	<?php endif; ?>
	<?php do_action( 'fl_page_close' ); ?>
</div><!-- .fl-page -->
<?php

wp_footer();

do_action( 'fl_body_close' );

FLTheme::footer_code();

?>
<?php /* ?>
<div id="popup-style" class="hidden">
<div class="calendly-inline-widget" data-url="https://calendly.com/gwfdesign/gwf-virtual-design-appointment?primary_color=7bc051" style="min-width:320px;height:630px;"></div>
<script type="text/javascript" src="https://assets.calendly.com/assets/external/widget.js"></script>
</div>
<?php */ ?>

<script src="https://calculator.measuresquare.com/scripts/jquery-m2FlooringCalculator.js"></script>
<script>
            $(function () {
                $('#calculateBtn').m2Calculator({
                    measureSystem: "Imperial",            
                    thirdPartyName: "Tish Flooring", 
                    thirdPartyEmail: "devteam@mobile-marketing.agency",  // if showDiagram = false, will send estimate data to this email when user click Email Estimate button
                    showCutSheet: false, // if false, will not include cutsheet section in return image
                    showDiagram: true,  // if false, will close the popup directly 
                  /*  product: {
                        type: "Carpet",
                        name: "Carpet 1",
                        width: "6'0\"",
                        length: "150'0\"",
                        horiRepeat: "3'0\"",
                        vertRepeat: "3'0\"",
                        horiDrop: "",
                        vertDrop: ""
                    },
					*/
                    cancel: function () {
                        //when user closes the popup without calculation.
                    },
                    callback: function (data) {
                        //json format, include user input, usage and base64image
                        $("#callback").html(JSON.stringify(data));   
                        console.log(data.input)
                        $("#usageText").val(data.usage);    
                        $("#image").attr("src", data.img);  //base64Image 
						window.location.href = "https://tishflooring-stg.mm-dev.agency/thank-you";
                    }
                });
            });
         </script>
</body>
</html>
